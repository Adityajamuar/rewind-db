"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
      .describeTable("room")
      .then(() => {
        return queryInterface.addColumn("room", "name", {
          type: Sequelize.STRING,
          allowNull: false
        });
      })
      .catch(() => {
        //
      });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface
      .describeTable("users")
      .then(() => {
        return queryInterface.removeColumn("users", "name");
      })
      .catch(() => {
        //
      });
  }
};
