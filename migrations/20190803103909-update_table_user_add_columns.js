"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
      .describeTable("users")
      .then(() => {
        return queryInterface.addColumn("users", "name", {
          type: Sequelize.STRING,
          allowNull: false
        });
      })
      .then(() => {
        return queryInterface.addColumn("users", "in_game", {
          type: Sequelize.BOOLEAN,
          defaultValue: false
        });
      })
      .then(() => {
        return queryInterface.addColumn("users", "online", {
          type: Sequelize.BOOLEAN,
          defaultValue: false
        });
      })
      .catch(() => {
        //
      });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface
      .describeTable("users")
      .then(() => {
        return queryInterface.removeColumn("users", "name");
      })
      .then(() => {
        return queryInterface.removeColumn("users", "in_game");
      })
      .then(() => {
        return queryInterface.removeColumn("users", "online");
      })
      .catch(() => {
        //
      });
  }
};
