const createLanguageModel = require("../models/language.model");

module.exports = function(context) {
  const languageModel = createLanguageModel(context.app);

  languageModel
    .destroy({
      where: {
        userId: context.result.id
      }
    })
    .catch(err => {
      throw new Error(err);
    });
};
