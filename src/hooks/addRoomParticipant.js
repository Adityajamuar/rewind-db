const createRoomParticipantModel = require("../models/participants.model");

module.exports = function(context) {
  const roomParticipantModel = createRoomParticipantModel(context.app);

  roomParticipantModel
    .create({
      room_id: context.result.id,
      user_id: context.params.user.id
    })
    .catch(err => {
      throw new Error(err);
    });
};
