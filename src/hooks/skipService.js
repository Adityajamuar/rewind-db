const errors = require("@feathersjs/errors");

module.exports = function() {
  throw new errors[401]("You are not authorised to access this end-point.");
};
