const createRoomParticipantModel = require("../models/participants.model");
const createRoomModel = require("../models/room.model");

module.exports = async function(context) {
  const participantModel = createRoomParticipantModel(context.app);
  const roomModel = createRoomModel(context.app);

  let maxLength = await roomModel
    .findOne({
      where: {
        id: context.data.room_id
      }
    })
    .then(room => {
      return room.dataValues.max_members;
    });

  await participantModel
    .findAll({
      where: {
        room_id: context.data.room_id
      }
    })
    .then(result => {
      if (result.length + 1 === maxLength) {
        throw new Error("Room is full");
      } else {
        return context;
      }
    })
    .catch(err => {
      throw new Error(err);
    });
};
