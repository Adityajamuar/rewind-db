const createRoomModel = require("../models/room.model");
const createRoomParticipantModel = require("../models/participants.model");

module.exports = async function(context) {
  const roomModel = createRoomModel(context.app);
  const roomParticipantModel = createRoomParticipantModel(context.app);

  await roomParticipantModel
    .findAll({
      where: {
        id: context.result.room_id
      }
    })
    .then(result => {
      if (result.length === 0) {
        roomModel
          .destroy({
            where: {
              id: context.result.room_id
            }
          })
          .catch(err => {
            throw new Error(err);
          });
      }
    })
    .catch(err => {
      throw new Error(err);
    });
};
