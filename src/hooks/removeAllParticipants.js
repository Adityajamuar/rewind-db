const createRoomParticipantModel = require("../models/participants.model");

module.exports = function(context) {
  const roomParticipantModel = createRoomParticipantModel(context.app);

  roomParticipantModel
    .destroy({
      where: {
        room_id: context.result.id
      }
    })
    .catch(err => {
      throw new Error(err);
    });
};
