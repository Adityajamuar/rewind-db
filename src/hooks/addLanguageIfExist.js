const createLanguageModel = require("../models/language.model");

module.exports = function(context) {
  const languageModel = createLanguageModel(context.app);

  languageModel
    .create({
      userId: context.result.id,
      language: context.data.language
    })
    .catch(err => {
      throw new Error(err);
    });
};
