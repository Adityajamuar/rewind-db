const { authenticate } = require("@feathersjs/authentication").hooks;
const addRoomParticipant = require("../../hooks/addRoomParticipant");
const removeAllParticipants = require("../../hooks/removeAllParticipants");
const addAssociations = require("../../hooks/addAssociations");

module.exports = {
  before: {
    all: [authenticate("jwt")],
    find: [
      addAssociations({
        models: [
          {
            model: "participants",
            as: "participants"
          }
        ]
      })
    ],
    get: [
      addAssociations({
        models: [
          {
            model: "participants",
            as: "participants"
          }
        ]
      })
    ],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [addRoomParticipant],
    update: [],
    patch: [],
    remove: [removeAllParticipants]
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
