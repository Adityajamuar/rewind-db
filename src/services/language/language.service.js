// Initializes the `language` service on path `/language`
const createService = require('feathers-sequelize');
const createModel = require('../../models/language.model');
const hooks = require('./language.hooks');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/language', createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service('language');

  service.hooks(hooks);
};
