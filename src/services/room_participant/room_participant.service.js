// Initializes the `room_participant` service on path `/room-participant`
const createService = require('feathers-sequelize');
const createModel = require('../../models/room_participant.model');
const hooks = require('./room_participant.hooks');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/room-participant', createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service('room-participant');

  service.hooks(hooks);
};
