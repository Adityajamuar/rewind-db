const users = require('./users/users.service.js');
const game = require('./game/game.service.js');
const room = require('./room/room.service.js');
const roomParticipant = require('./room_participant/room_participant.service.js');
const language = require('./language/language.service.js');
const participants = require('./participants/participants.service.js');
const getMyId = require('./get-my-id/get-my-id.service.js');
// eslint-disable-next-line no-unused-vars
module.exports = function (app) {
  app.configure(users);
  app.configure(game);
  app.configure(room);
  app.configure(roomParticipant);
  app.configure(language);
  app.configure(participants);
  app.configure(getMyId);
};
