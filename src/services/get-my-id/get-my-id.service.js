// Initializes the `get-my-id` service on path `/get-my-id`
const createService = require('./get-my-id.class.js');
const hooks = require('./get-my-id.hooks');

module.exports = function (app) {
  
  const paginate = app.get('paginate');

  const options = {
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/get-my-id', createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service('get-my-id');

  service.hooks(hooks);
};
