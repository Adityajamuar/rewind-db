const { authenticate } = require("@feathersjs/authentication").hooks;
const commonHooks = require("feathers-hooks-common");
const addAssociations = require("../../hooks/addAssociations");
const checkIfUser = require("../../hooks/checkIfUser");
const checkIfUserWhenDeleted = require("../../hooks/checkIfUserWhenDeleted");
const skipService = require("../../hooks/skipService");
const addLanguageIfExist = require("../../hooks/addLanguageIfExist");
const removeLanguage = require("../../hooks/removeLanguage.js");

const {
  hashPassword,
  protect
} = require("@feathersjs/authentication-local").hooks;

module.exports = {
  before: {
    all: [],
    find: [authenticate("jwt")],
    get: [
      authenticate("jwt"),
      addAssociations({
        models: [
          {
            model: "language",
            as: "languages"
          }
        ]
      })
    ],
    create: [hashPassword()],
    update: [
      hashPassword(),
      authenticate("jwt"),
      commonHooks.iff(checkIfUser).else(skipService)
    ],
    patch: [
      hashPassword(),
      authenticate("jwt"),
      commonHooks.iff(checkIfUser).else(skipService)
    ],
    remove: [
      authenticate("jwt"),
      commonHooks.iff(checkIfUserWhenDeleted).else(skipService)
    ]
  },

  after: {
    all: [
      // Make sure the password field is never sent to the client
      // Always must be the last hook
      protect("password")
    ],
    find: [],
    get: [],
    create: [addLanguageIfExist],
    update: [],
    patch: [],
    remove: [removeLanguage]
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
