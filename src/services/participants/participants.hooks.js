const { authenticate } = require("@feathersjs/authentication").hooks;
const deleteRoom = require("../../hooks/deleteRoom");
const checkIfPlaceLeft = require("../../hooks/checkIfPlaceLeft");

module.exports = {
  before: {
    all: [authenticate("jwt")],
    find: [],
    get: [],
    create: [checkIfPlaceLeft],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [deleteRoom]
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
