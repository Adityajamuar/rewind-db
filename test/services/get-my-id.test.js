const assert = require('assert');
const app = require('../../src/app');

describe('\'get-my-id\' service', () => {
  it('registered the service', () => {
    const service = app.service('get-my-id');

    assert.ok(service, 'Registered the service');
  });
});
