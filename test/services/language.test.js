const assert = require('assert');
const app = require('../../src/app');

describe('\'language\' service', () => {
  it('registered the service', () => {
    const service = app.service('language');

    assert.ok(service, 'Registered the service');
  });
});
