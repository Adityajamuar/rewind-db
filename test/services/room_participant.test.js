const assert = require('assert');
const app = require('../../src/app');

describe('\'room_participant\' service', () => {
  it('registered the service', () => {
    const service = app.service('room-participant');

    assert.ok(service, 'Registered the service');
  });
});
